##
## Makefile for SoundEngine in /home/tosi_t//Prog/C++/BomberMan/Sound
## 
## Made by timothee tosi
## Login   <tosi_t@epitech.net>
## 
## Started on  Tue May 28 16:58:19 2013 timothee tosi
## Last update Mon Sep 30 21:43:21 2013 timothee tosi
##

CC		=	g++

RM		=	rm -rf

CPPFLAGS	+=	-Wall -Wextra -Werror
CPPFLAGS	+=	-I./inc/

LDFLAGS		=	-lSDL -lSDL_mixer

NAME		=	ToIsengard

SRC		=	src/main.cpp	\
			src/SoundEngine.cpp

OBJ		=	$(SRC:.cpp=.o)

all:		$(NAME)

$(NAME):	$(OBJ)
		$(CC) $(OBJ) -static-libgcc -o $(NAME) $(LDFLAGS)

clean:
		$(RM) $(OBJ)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re