//
// Sound.hh for Sound in /home/tosi_t//Prog/C++/BomberMan/Sound/inc
// 
// Made by timothee tosi
// Login   <tosi_t@epitech.net>
// 
// Started on  Tue May 28 16:25:19 2013 timothee tosi
// Last update Sat Jun  1 17:48:35 2013 timothee tosi
//

#ifndef				SOUNDENGINE_HH_
# define			SOUNDENGINE_HH_

# include			<SDL/SDL.h>
# include			<SDL/SDL_mixer.h>
# include			<algorithm>
# include			<string>
# include			<vector>

class				SoundEngine
{
private:
  std::vector<Mix_Chunk*>	FX_;
  std::vector<Mix_Music*>	track_;
  SoundEngine(const SoundEngine & _rho);
  SoundEngine &			operator=(const SoundEngine & _rho);

public:
  SoundEngine();
  ~SoundEngine();
  bool				init(const unsigned int channel);
  bool				loadTrack(const std::string & path);
  bool				loadFX(const std::string & path);
  bool				playFX(const unsigned int number, const unsigned int channel);
  bool				playTrack(const unsigned int number, const int loop);
  bool				fadeIn(const unsigned int number, const int loop, const int time);
  bool				fadeOut(const int time);
  void				muteTrack(void);
  void				muteFX(void);
  void				muteAll(void);
  void				unmuteTrack(void);
  void				unmuteFX(void);
  void				unmuteAll(void);
};

#endif				/* !SOUNDENGINE_HH_ */
