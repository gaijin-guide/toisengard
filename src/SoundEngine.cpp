//
// SounEngine.cpp for SoundEngine in /home/tosi_t//Prog/C++/BomberMan/Sound/src
// 
// Made by timothee tosi
// Login   <tosi_t@epitech.net>
// 
// Started on  Tue May 28 16:28:38 2013 timothee tosi
// Last update Sat Jun  1 17:49:28 2013 timothee tosi
//

#include		"SoundEngine.hh"

SoundEngine::SoundEngine(){};

SoundEngine::~SoundEngine()
{
  for_each(track_.begin(), track_.end(), Mix_FreeMusic);
  for_each(FX_.begin(), FX_.end(), Mix_FreeChunk);
  Mix_CloseAudio();
  SDL_Quit();
}

bool			SoundEngine::init(const unsigned int channel)
{
  if ((SDL_Init(SDL_INIT_AUDIO)) == 1)
    return (false);
  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024)
      == -1)
    return (false);
  Mix_AllocateChannels(channel);
  return (true);
}

bool			SoundEngine::loadTrack(const std::string & path)
{
  Mix_Music *		track;

  if ((track = Mix_LoadMUS(path.c_str())) == NULL)
    return (false);
  track_.push_back(track);
  return (true);
}

bool			SoundEngine::loadFX(const std::string & path)
{
  Mix_Chunk *		FX;

  if ((FX = Mix_LoadWAV(path.c_str())) == NULL)
    return (false);
  FX_.push_back(FX);
  return (true);
}

bool			SoundEngine::playFX(const unsigned int FX, const unsigned int channel)
{
  if ((Mix_PlayChannel(channel, FX_[FX - 1], 0)) == -1)
    return (false);
  return (true);
}

bool			SoundEngine::playTrack(const unsigned int track, const int loop)
{
  if ((Mix_PlayMusic(track_[track - 1], (loop - 1))) == -1)
    return (false);
  return (true);
}

bool			SoundEngine::fadeIn(const unsigned int track, const int loop, const int time)
{
  fadeOut(time / 2);
  if ((Mix_FadeInMusic(track_[track - 1], (loop - 1), time)) == -1)
    return (false);
  return (true);
}

bool			SoundEngine::fadeOut(const int time)
{
  if ((Mix_FadeOutMusic(time)) == 0)
    return (false);
  return (true);
}

void			SoundEngine::muteTrack(void)
{
  Mix_VolumeMusic(0);
}

void			SoundEngine::muteFX(void)
{
  for (std::vector<Mix_Chunk*>::iterator it = FX_.begin(); it != FX_.end(); ++it)
    Mix_VolumeChunk(*it, 0);
}

void			SoundEngine::muteAll(void)
{
  muteFX();
  muteTrack();
}

void			SoundEngine::unmuteTrack(void)
{
  Mix_VolumeMusic(MIX_MAX_VOLUME);
}

void			SoundEngine::unmuteFX(void)
{
  for (std::vector<Mix_Chunk*>::iterator it = FX_.begin(); it != FX_.end(); ++it)
    Mix_VolumeChunk(*it, MIX_MAX_VOLUME);
}

void			SoundEngine::unmuteAll(void)
{
  unmuteFX();
  unmuteTrack();
}
