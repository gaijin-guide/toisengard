//
// main.cpp for main in /home/tosi_t//Prog/C++/BomberMan/Sound/src
// 
// Made by timothee tosi
// Login   <tosi_t@epitech.net>
// 
// Started on  Tue May 28 16:56:19 2013 timothee tosi
// Last update Mon Sep 30 22:08:38 2013 timothee tosi
//

#include		<iostream>
#include		"SoundEngine.hh"

int			main()
{
  SoundEngine		sound;

  if (!sound.init(2))
    return (false);
  sound.loadTrack("sound/ToIsengard.ogg");
  sound.playTrack(1, 1);
  while (1);
  return (true);
}
